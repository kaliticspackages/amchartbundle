<?php
/**
 * Created by PhpStorm.
 * User: igor
 * Date: 31.08.17
 * Time: 1:09
 */

namespace IK\AmChartsBundle\Charts\Components;


class CategoryField implements \JsonSerializable {
    public $categoryField;
    public function __construct($categoryField) {
        $this->categoryField = $categoryField;
    }

    public function jsonSerialize() {
        return $this->categoryField;
    }

}